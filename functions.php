<?php

/**
 *
 * Please do not make any edits to this file. All edits should be done in a child theme.
 *
 * @package Kentaurus
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

//Define theme version
define('KENTAURUS_VER', wp_get_theme()->get('Version'));

//Define template directory
define('KENTAURUS_TEMPLATE_DIR', get_template_directory());

//Define template directory uri
define('KENTAURUS_DIR_URI', get_template_directory_uri());

define('DEVELOPMENT', true);

//Include theme setup files
require_once KENTAURUS_TEMPLATE_DIR . '/inc/casinotheme-setup.php';

//Casino custom post type
require_once KENTAURUS_TEMPLATE_DIR . '/inc/casinotheme-casinos-cpt.php';

//General code for theme - scripts, styles
require_once KENTAURUS_TEMPLATE_DIR . '/inc/casinotheme-general.php';

//Helper functions
require_once KENTAURUS_TEMPLATE_DIR . '/inc/casinotheme-helper-functions.php';

//Include ACF setup files
require_once KENTAURUS_TEMPLATE_DIR . '/inc/casinotheme-acf-setup.php';

//Include ACF fields for theme
require_once KENTAURUS_TEMPLATE_DIR . '/inc/casinotheme-acf-fields.php';

//Include this file if remove slug option is enabled
if (get_field('casino_remove_slug', 'options')) {
    require_once KENTAURUS_TEMPLATE_DIR . '/inc/casinotheme-remove-casino-slug.php';
}
