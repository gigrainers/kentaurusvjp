<?php

/**
 * The template for displaying archives.
 * 
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

$author_id = get_the_author_meta('ID');
$author_name = get_field('author_name', 'user_' . $author_id);
$author_image = get_field('author_image', 'user_' . $author_id);

?>

<main class="site-main" role="main">

    <div id="ajax-posts" class="archive-content">
        <div class="container">
            <h1 class="my-10"><?php _e('News Archive', 'kentaurus') ?></h1>
            <div class="inner-archive-content">
                <?php
                $args = array(
                    'post_type' => 'news',
                    'posts_per_page' => 18,
                );

                $news_posts = new WP_Query($args);

                if ($news_posts->have_posts()) :
                    while ($news_posts->have_posts()) : $news_posts->the_post(); ?>

                        <?php get_template_part( 'template-parts/news-archive-post' ); ?>

                <?php endwhile;
                else :
                    _e('Sorry, no posts found', 'kentaurus');
                endif;
                ?>
            </div>
                <button class="btn load-more"><?php _e('Load More Posts', 'kentaurus'); ?></button>
        </div>
    </div>
</main>