<?php

/**
 * The template for displaying 404 page.
 * 
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}
?>

<main class="site-main" role="main">
    <div class="page-404">
        <div class="container">
            <div class="page-404-inner">
                <div class="page-404-image">
                    <img src="<?php echo KENTAURUS_DIR_URI . '/dist/img/404-img.png'; ?>" alt="image-404-page">
                </div>
                <div class="page-404-info">
                    <h1 class="title-404"><?= 404 ?></h1>
                    <span class="subtitle-404"><?php _e('UH OH! You are lost.', 'kentaurus'); ?></span>
                    <p class="text-404"><?php _e('The page you are looking for does not exist. Click below to return to the homepage.', 'kentaurus'); ?></p>
                    <button class="btn"><a href="/"><?php _e('Return to the homepage', 'kentaurus'); ?></a></button>
                </div>
            </div>
        </div>
    </div>
</main>