<?php

/**
 * The template for displaying casino archive page
 * 
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

?>

<main class="site-main" role="main">
    <div class="page-block">
        <div class="casino-archive container">
            <div class="inner-casino-archive">
                <?php the_field('casino_archive_page_conent', 'options'); ?>
            </div>
        </div>
    </div>
</main>