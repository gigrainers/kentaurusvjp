<?php

/**
 * Simple page header block
 *
 * @package Kentaurus
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

?>

<div class="casino-list3-item3 base-style3" <?php if (get_field('enable_terms_and_conditions')) {
                                                echo 'style="_toplist-base3.scss"';
                                            } else {
                                                echo 'style="margin-bottom:50px;"';
                                            } ?>>
    <span class="casino-list3__order3"></span>
    <div class="casino-list3__logo3">
        <a href="<?php the_field('casino_redirect_link'); ?>">
            <?php the_post_thumbnail('toplist-size'); ?>
        </a>
    </div>
    <div class="casino-list3__bonus3">
        <?php the_field('casino_toplist_bonus_line'); ?>
    </div>
    <div class="casino-list3__rating3">
        <span class="casino-name3"><?php the_title(); ?></span>
        <div class="star-rating3"><i class="star"></i></div>
        <div class="casino-rating3">
            <?php if (get_field('casino_toplist_rating')) : ?>
                <?php the_field('casino_toplist_rating'); ?>
            <?php else : ?>
                <?php echo '-'; ?>
            <?php endif; ?>

        </div>
    </div>
    <div class="casino-list3__pluses3">
        <?php
        if (have_rows('casino_top_3')) :
            while (have_rows('casino_top_3')) : the_row(); ?>
                <span class="plus3"><i><span class="checkmark">
                            <div class="checkmark_stem"></div>
                            <div class="checkmark_kick"></div>
                        </span></i><?php the_sub_field('top_three_line'); ?></span>
        <?php
            endwhile;
        endif;
        ?>
    </div>
    <div class="casino-list3__more3">
        <a href="<?php the_field('casino_redirect_link'); ?>" rel="nofollow noopener" target="_blank" class="to-the-casino3"><?php _e('Get Bonus!', 'kentaurus'); ?></a>
        <a href="<?php the_permalink(); ?>" class="list-review3"><?php _e('Casino Review', 'kentaurus'); ?> <i class="arrow arrow-right"></i></a>
    </div>
    <?php if (get_field('enable_terms_and_conditions')) : ?>
        <div class="casino-list3__terms3">
            <i class="fas fa-info-circle"></i><span><?php the_field('terms_and_conditions'); ?></span>
        </div>
    <?php endif; ?>
</div>