<?php

/**
 * Simple page header block
 *
 * @package Kentaurus
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

?>

<div class="casino-list4-item4 base-style4" <?php if (get_field('enable_terms_and_conditions')) {
                                                echo 'style="_toplist-base4.scss"';
                                            } else {
                                                echo 'style="margin-bottom:50px;"';
                                            } ?>>
    <!-- <span class="casino-list4__order4"></span> -->
    <div class="casino-list4__logo4">
        <a href="<?php the_field('casino_redirect_link'); ?>">
            <?php the_post_thumbnail('toplist-size2'); ?>
        </a>
    </div>
    <div class="casino-list4__rating4">
        <span class="casino-name4"><?php the_title(); ?></span>
        <div class="casino-list4__bonus4">
            <?php the_field('casino_toplist_bonus_line'); ?>
        </div>
    </div>
    <div class="casino-list4__pluses4">
        <?php
        if (have_rows('casino_top_4')) :
            while (have_rows('casino_top_4')) : the_row(); ?>
                <span class="plus4"><i><span class="checkmark">
                            <div class="checkmark_stem"></div>
                            <div class="checkmark_kick"></div>
                        </span></i><?php the_sub_field('top_three_line'); ?></span>
        <?php
            endwhile;
        endif;
        ?>
        <div class="casino-list4__bonus4">
            <div class="star-rating4"><i class="star"></i></div>
            <div class="casino-rating4">
                <?php if (get_field('casino_toplist_rating')) : ?>
                    <?php the_field('casino_toplist_rating'); ?>
                <?php else : ?>
                    <?php echo '-'; ?>
                <?php endif; ?>
            </div>
            <a href="<?php the_field('casino_redirect_link'); ?>" rel="nofollow noopener" target="_blank" class="to-the-casino4"><?php _e('Get Bonus!', 'kentaurus'); ?></a>
            <a href="<?php the_permalink(); ?>" class="list-review4"><?php _e('Casino Review', 'kentaurus'); ?> <i class="arrow arrow-right"></i></a>
        </div>
        <?php if (get_field('enable_terms_and_conditions')) : ?>
            <div class="casino-list4__terms4">
                <i class="fas fa-info-circle"></i><span><?php the_field('terms_and_conditions'); ?></span>
            </div>
        <?php endif; ?>
    </div>
</div>