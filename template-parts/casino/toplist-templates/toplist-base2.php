<?php

/**
 * Simple page header block
 *
 * @package Kentaurus
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

?>

<div class="casino-list2-item2 base-style2" <?php if (get_field('enable_terms_and_conditions')) {
                                                echo 'style="_toplist-base2.scss"';
                                            } else {
                                                echo 'style="margin-bottom:50px;"';
                                            } ?>>
    <span class="casino-list2__order2"></span>
    <div class="casino-list2__logo2">
        <a href="<?php the_field('casino_redirect_link'); ?>">
            <?php the_post_thumbnail('toplist-size'); ?>
        </a>
    </div>
    <div class="casino-list2__bonus2">
            <?php the_field('casino_toplist_bonus_line'); ?>
    </div>
</div>