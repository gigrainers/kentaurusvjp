<?php

/**
 * Simple page header block
 *
 * @package Kentaurus
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

?>
<!-- Header block for front page -->
<?php if (is_front_page()) : ?>
    <div class="header-block" style="background-image: url(<?php echo KENTAURUS_DIR_URI . '/dist/img/BGelements/Strap.png' ?>);">
        <div class="header-block-inner container">
            <?php get_template_part('template-parts/header-block-parts/front-page-content'); ?>
        </div>
    </div>
    <!-- End Header block for front page -->

    <!-- Header block for simple page -->
<?php elseif (is_singular('page')) : ?>
    <div class="header-block simple-page" style="background-image: url(<?php echo KENTAURUS_DIR_URI . '/dist/img/BGelements/Strap.png' ?>);">
        <div class="header-block-inner container">
            <?php get_template_part('template-parts/header-block-parts/single-page-content'); ?>
        </div>
    </div>
    <?php get_template_part('template-parts/header-block-parts/page-navigation'); ?>
    <!-- Casino Archive header styles -->

<?php else : ?>
    <div class="header-block simple-page" style="background-image: url(<?php echo KENTAURUS_DIR_URI . '/dist/img/BGelements/Strap.png' ?>);">
        <div class="header-block-inner container">
            <?php if (is_post_type_archive('casino')) : ?>
                <?php get_template_part('template-parts/header-block-parts/archive-page-header'); ?>
            <?php else : ?>
                <?php get_template_part('template-parts/header-block-parts/post-page-content'); ?>
            <?php endif; ?>
        </div>
    </div>
    <?php get_template_part('template-parts/header-block-parts/page-navigation'); ?>
<?php endif; ?>
<!-- End Header block for simple page -->